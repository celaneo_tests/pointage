import "dotenv/config";
import express from "express";
import morgan from "morgan";
import bodyParser from "body-parser";
import swaggerUi from "swagger-ui-express";

import { connectDB } from "./src/utils/database.js";
import allRoutes from "./src/routes/index.js";
import swaggerOptions from "./config/swaggerOptions.js";

const app = express();
const port = process.env.PORT || 3000;

// Middleware for redirection
app.use((req, res, next) => {
  if (req.url === "/") {
    res.redirect(301, "/api");
  } else {
    next();
  }
});

// Serve Swagger UI
app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerOptions));

// Middleware
app.use(morgan("dev"));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// Connect to MongoDB
connectDB();

// Routes
app.use("/api", allRoutes);

// Start server
app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});

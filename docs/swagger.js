/* Swagger configuration */
/*const options = {
  version: "2.0",
  openapi: "3.0.0", // Enable/Disable OpenAPI. By default is null
  language: "en-US", // Change response language. By default is 'en-US'
  disableLogs: false, // Enable/Disable logs. By default is false
  autoHeaders: false, // Enable/Disable automatic headers capture. By default is true
  autoQuery: false, // Enable/Disable automatic query capture. By default is true
  autoBody: false, // Enable/Disable automatic body capture. By default is true
};*/

import swaggerAutogen from "swagger-autogen";
import { HTTP_RESPONSE_CODES } from "../src/utils/constants.js";

const doc = {
  openapi: "3.0.0",
  info: {
    version: "1.0.0",
    title: "Pointage",
    description: "Application de pointage des employés d'une école",
    contact: {
      name: "Houcem Eddine SANAI",
      email: "sanai.houcem@gmail.com",
    },
  },
  basePath: "/",
  schemes: ["http"],
  consumes: ["application/json"],
  produces: ["application/json"],
  definitions: {
    healthResponse: {
      code: 200,
      message: HTTP_RESPONSE_CODES[200],
    },
    "errorResponse.400": {
      code: 400,
      message: HTTP_RESPONSE_CODES[400],
    },
    "errorResponse.403": {
      code: 403,
      message: HTTP_RESPONSE_CODES[403],
    },
    "errorResponse.404": {
      code: 404,
      message: HTTP_RESPONSE_CODES[404],
    },
    "errorResponse.500": {
      code: 500,
      message: HTTP_RESPONSE_CODES[500],
    },
  }, // by default: empty object (Swagger 2.0)
};

const outputFile = "./docs/swagger.json";
const endpointsFiles = ["./index.js", "./controllers/*.js"];

/* NOTE: if you use the express Router, you must pass in the 
   'endpointsFiles' only the root file where the route starts,
   such as: index.js, app.js, routes.js, ... */
swaggerAutogen(outputFile, endpointsFiles, doc);

// swaggerAutogen(outputFile, endpointsFiles, doc).then(() => {
//     require('./index.js'); // Your project's root file
//   });

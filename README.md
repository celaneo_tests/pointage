# Celaneo ~ Pointage

C'est une application de pointage des employés dans une école. Le but est de suivre le pointage des employés d'une façon efficace et sécurisée.

## Environment Variables

To run this project, you will need to add the following environment variables to your .env file

`MONGODB_URI` : Mongo databse URI

## Run Locally

Clone the project

```bash
  git clone https://gitlab.com/celaneo_tests/pointage
```

Go to the project directory

```bash
  cd pointage
```

Install dependencies

```bash
  yarn install
```

Start the server

```bash
  yarn dev
```

## Running Tests

To run tests, run the following command

```bash
    yarn test
```

## Docker & Deployment

To create docker image run :

```bash
  yarn make
```

To run the image run :

```bash
  yarn make_run
```

To push the image to Docker Hub run :

```bash
  yarn make_push
```

## Tech Stack

**Server:** Node, Express, Mongoose

## Support

For support, email sanai.houcem@gmail.com or call directly on +21651882250

## Authors

- [@sanaiHoucem](http://linkedin.com/in/houssem-eddine-sanai/)

## License

[MIT](https://choosealicense.com/licenses/mit/)

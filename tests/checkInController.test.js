import { MongoMemoryServer } from "mongodb-memory-server";
import mongoose from "mongoose";
import { validationResult } from "express-validator";
import Employee from "../src/models/employeeModel.js";
import { checkIn, checkOut } from "../src/controllers/checkInController.js";
import {
  HTTP_RESPONSE_CODES,
  RESPONSE_MESSAGES,
} from "../src/utils/constants.js";

jest.mock("express-validator");
jest.mock("../src/models/employeeModel.js");

let mongod;

beforeAll(async () => {
  mongod = await MongoMemoryServer.create();
  const uri = mongod.getUri();
  await mongoose.connect(uri);
});

afterAll(async () => {
  await mongoose.disconnect();
  await mongod.stop();
});

describe("checkIn", () => {
  it("should handle validation errors and return 400 with error details", async () => {
    validationResult.mockReturnValueOnce({
      isEmpty: () => false,
      array: () => [
        {
          type: "field",
          msg: "EmployeeId is required",
          path: "employeeId",
          location: "body",
        },
        {
          type: "field",
          msg: "Comment is required",
          path: "comment",
          location: "body",
        },
      ],
    });

    const req = { body: {} };
    const res = { status: jest.fn().mockReturnThis(), json: jest.fn() };

    // Call the checkIn function
    await checkIn(req, res);

    // Assert the expected response
    expect(res.status).toHaveBeenCalledWith(400);
    expect(res.json).toHaveBeenCalledWith({
      code: "Bad Request",
      errors: [
        {
          type: "field",
          msg: "EmployeeId is required",
          path: "employeeId",
          location: "body",
        },
        {
          type: "field",
          msg: "Comment is required",
          path: "comment",
          location: "body",
        },
      ],
    });
  });

  it("should successfully check-in an employee and return 201 with check-in details", async () => {
    const req = { body: { employeeId: "1", comment: "Check-in comment" } };
    const res = { status: jest.fn().mockReturnThis(), json: jest.fn() };

    Employee.findOne.mockResolvedValueOnce({
      _id: "657e0c1cd6ed2dd5b5bdcbbf",
      id: "1",
      name: "Houcem",
      firstName: "Houcem",
      dateCreated: "2023-12-16",
      department: "INFO",
      __v: 0,
      workHours: "Working..",
    });

    await checkIn(req, res);

    expect(res.status).toHaveBeenCalledWith(201);
    expect(res.json).toHaveBeenCalledWith({
      code: HTTP_RESPONSE_CODES[201],
      message: RESPONSE_MESSAGES.CHECK_IN_SUCCESS,
      data: expect.any(Object),
    });
  });
});

describe("checkOut", () => {
  it("should handle validation errors and return 400 with error details", async () => {
    validationResult.mockReturnValueOnce({
      isEmpty: () => false,
      array: () => [{ msg: "Validation error" }],
    });

    const req = { body: {} };
    const res = { status: jest.fn().mockReturnThis(), json: jest.fn() };

    await checkOut(req, res);

    expect(res.status).toHaveBeenCalledWith(400);
    expect(res.json).toHaveBeenCalledWith({
      errors: [{ msg: "Validation error" }],
    });
  });

  it("should successfully check-out an employee and return 201 with check-out details", async () => {
    const req = { body: { employeeId: "1", comment: "Check-out comment" } };
    const res = { status: jest.fn().mockReturnThis(), json: jest.fn() };

    Employee.findOne = jest.fn().mockResolvedValueOnce({
      _id: "657e0c1cd6ed2dd5b5bdcbbf",
      id: "1",
      name: "Houcem",
      firstName: "Houcem",
      dateCreated: "2023-12-16",
      department: "INFO",
      __v: 0,
      workHours: "Working..",
    });

    await checkOut(req, res);

    expect(res.status).toHaveBeenCalledWith(201);
    expect(res.json).toHaveBeenCalledWith({
      code: HTTP_RESPONSE_CODES[201],
      message: RESPONSE_MESSAGES.CHECK_OUT_SUCCESS,
      data: expect.any(Object),
    });
  });
});

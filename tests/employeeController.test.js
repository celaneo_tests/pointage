import { MongoMemoryServer } from "mongodb-memory-server";
import mongoose from "mongoose";

import {
  createEmployee,
  listEmployees,
} from "../src/controllers/employeeController";
import { RESPONSE_MESSAGES } from "../src/utils/constants";

jest.useFakeTimers({ legacyFakeTimers: true });

let mongod;

beforeAll(async () => {
  mongod = await MongoMemoryServer.create();
  const uri = mongod.getUri();
  await mongoose.connect(uri);
});

afterAll(async () => {
  await mongoose.disconnect();
  await mongod.stop();
});

describe("CreateEmployee", () => {
  it("Should create a new employee", async () => {
    const req = {
      body: { id: "1", name: "John Doe", firstName: "John", department: "IT" },
    };
    const res = {
      status: jest.fn(() => res),
      json: jest.fn(),
    };

    await createEmployee(req, res);

    expect(res.status).toHaveBeenCalledWith(201);
    expect(res.json).toHaveBeenCalled();
  });

  it("Should fail when department is missing", async () => {
    const req = {
      body: { id: "2", name: "Jane Doe", firstName: "Jane" }, // department is missing
    };
    const res = {
      status: jest.fn(() => res),
      json: jest.fn(),
    };

    await createEmployee(req, res);

    expect(res.status).toHaveBeenCalledWith(500); // Expecting a 500 status for bad request
    expect(res.json).toHaveBeenCalled();
    expect(res.json.mock.calls[0][0].error).toContain(
      RESPONSE_MESSAGES.INTERNAL_SERVER_ERROR
    );
  });

  it("Should fail with an existing ID", async () => {
    const req = {
      body: { id: "1", name: "Jane Doe", firstName: "Jane", department: "IT" }, // Duplicate ID
    };
    const res = {
      status: jest.fn(() => res),
      json: jest.fn(),
    };

    await createEmployee(req, res);

    expect(res.status).toHaveBeenCalledWith(409); // Expecting a 500 status for bad request
    expect(res.json).toHaveBeenCalled();
    expect(res.json.mock.calls[0][0].error).toContain(
      RESPONSE_MESSAGES.EMPLOYEE_ALREADY_EXISTS
    );
  });
});

describe("Listing Employees", () => {
  it("Should list 1 employee", async () => {
    const req = {};
    const res = {
      status: jest.fn(() => res),
      json: jest.fn(),
    };

    await listEmployees(req, res);

    // Verify that the status is set to 200
    expect(res.status).toHaveBeenCalledWith(200);

    // Verify the content of the "employees" array
    const { employees } = res.json.mock.calls[0][0].data;
    expect(employees).toHaveLength(1); // The single employee inserted in the first test unit
    expect(employees[0]).toEqual(
      expect.objectContaining({
        id: expect.any(String),
        name: expect.any(String),
        firstName: expect.any(String),
        dateCreated: expect.any(String),
        department: expect.any(String),
      })
    );
  });
});

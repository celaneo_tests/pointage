import { Router } from "express";
import employeeRoutes from "./employeeRoutes.js";
import checkInOutRoutes from "./checkInOutRoutes.js";

const router = Router();

// Welcome route
router.get("/", (req, res) => {
  res.json({ message: "Hello, Welcome to Employee Time Clock App !" });
});

router.use("/employees", employeeRoutes);
router.use("/", checkInOutRoutes);

export default router;

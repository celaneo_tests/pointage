import express from "express";
import { checkInOutValidationRules } from "../utils/validationRules.js";
import { checkIn, checkOut } from "../controllers/checkInController.js";

const router = express.Router();

router.post("/check-in", checkInOutValidationRules, checkIn);
router.post("/check-out", checkInOutValidationRules, checkOut);

export default router;

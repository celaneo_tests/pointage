import express from "express";
import {
  createEmployee,
  listEmployees,
} from "../controllers/employeeController.js";
import { employeeValidationRules } from "../utils/validationRules.js";

const router = express.Router();

router.post("/create", employeeValidationRules, createEmployee);
router.get("/list", listEmployees);

export default router;

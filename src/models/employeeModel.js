import { mongoose } from "../utils/database.js";
import { Schema } from "mongoose";
import { currentDayFormatted } from "../utils/functions.js";
import { GLOBAL_CONSTANTS } from "../utils/constants.js";

const employeeSchema = new Schema({
  id: { type: String, required: true },
  name: { type: String, required: true },
  firstName: { type: String, required: true },
  dateCreated: { type: String, default: currentDayFormatted() },
  department: { type: String, required: true },
  workHours: { type: String, default: "Working.." },
});

const Employee = mongoose.model(GLOBAL_CONSTANTS.EMPLOYEE, employeeSchema);

export default Employee;

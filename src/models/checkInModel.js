import { mongoose } from "../utils/database.js";
import { Schema } from "mongoose";
import Employee from "./employeeModel.js";
import { GLOBAL_CONSTANTS } from "../utils/constants.js";

const checkInSchema = new Schema({
  type: { type: String, required: true, default: GLOBAL_CONSTANTS.CHECK_IN },
  employeeId: { type: String, required: true },
  comment: { type: String, required: true },
  creationDate: { type: Date, default: Date.now },
});

/**
 * Pre-save middleware for the CheckIn schema.
 *
 * This middleware is executed before saving a CheckIn document.
 * If the type of the CheckIn is "checkIn", it updates the workHours attribute
 * of the corresponding employee in the database.
 *
 * @function
 * @async
 * @param {function} next - The callback function to proceed with the save operation.
 * @returns {Promise<void>} - A Promise that resolves when the operation is complete.
 *
 * @throws {Error} If there is an error updating the employee workHours.
 */

checkInSchema.pre("save", async function (next) {
  if (this.type === GLOBAL_CONSTANTS.CHECK_IN) {
    try {
      const employee = await Employee.findOne({ id: this.employeeId });
      await Employee.updateOne(
        {
          _id: employee._id,
        },
        { $set: { workHours: "Working.." } }
      );
    } catch (error) {
      console.error("Error updating employee workHours:", error);
    } finally {
      next();
    }
  }
});

const CheckIn = mongoose.model(GLOBAL_CONSTANTS.CHECK_IN, checkInSchema);

export default CheckIn;

import { validationResult } from "express-validator";

import Employee from "../models/employeeModel.js";
import CheckIn from "../models/checkInModel.js";
import {
  currentDayFormatted,
  millisecondsToHours,
} from "../utils/functions.js";
import {
  GLOBAL_CONSTANTS,
  HTTP_RESPONSE_CODES,
  RESPONSE_MESSAGES,
} from "../utils/constants.js";

/**
 * Record a check-in for an employee.
 *
 * @function
 * @async
 * @param {Object} req - Express request object.
 * @param {Object} res - Express response object.
 * @returns {Promise<void>} - A Promise that resolves when the operation is complete.
 * @throws {Object} 400 - Bad Request: Validation errors.
 * @throws {Object} 404 - Not Found: Employee with the given ID is not found.
 * @throws {Object} 500 - Internal Server Error: An error occurred during the operation.
 *
 * @example
 * // Request
 * POST /api/checkin
 * {
 *   "employeeId": "12345",
 *   "comment": "Employee checked in successfully."
 * }
 *
 * // Response
 * {
 *   "message": "Check-in recorded successfully",
 *   "data": {
 *     "type": "checkIn",
 *     "employeeId": "12345",
 *     "comment": "Employee checked in successfully.",
 *     "creationDate": "2023-01-01T12:00:00.000Z"
 *   }
 * }
 */
const checkIn = async (req, res) => {
  try {
    // Check for validation errors
    const errors = validationResult(req);
    if (errors && !errors.isEmpty()) {
      return res.status(400).json({
        code: HTTP_RESPONSE_CODES[400],
        errors: errors.array(),
      });
    }

    const { employeeId, comment } = req.body;

    // Find the employee by employeeId
    const employee = await Employee.findOne({ id: employeeId });

    if (!employee) {
      return res.status(404).json({
        code: HTTP_RESPONSE_CODES[404],
        error: RESPONSE_MESSAGES.EMPLOYEE_NOT_FOUND,
      });
    }

    // Create a check-in record
    const newCheckIn = new CheckIn({
      employeeId,
      comment,
    });

    // Save the Check-in record to DB
    const savedCheckIn = await newCheckIn.save();

    res.status(201).json({
      code: HTTP_RESPONSE_CODES[201],
      message: RESPONSE_MESSAGES.CHECK_IN_SUCCESS,
      data: savedCheckIn,
    });
  } catch (error) {
    res.status(500).json({
      code: HTTP_RESPONSE_CODES[500],
      error: RESPONSE_MESSAGES.INTERNAL_SERVER_ERROR,
    });
  }
};

/**
 * Record a check-out for an employee.
 *
 * @function
 * @async
 * @param {Object} req - Express request object.
 * @param {Object} res - Express response object.
 * @returns {Promise<void>} - A Promise that resolves when the operation is complete.
 * @throws {Object} 400 - Bad Request: Validation errors.
 * @throws {Object} 404 - Not Found: Employee with the given ID is not found.
 * @throws {Object} 500 - Internal Server Error: An error occurred during the operation.
 *
 * @example
 * // Request
 * POST /api/checkout
 * {
 *   "employeeId": "12345",
 *   "comment": "Employee checked out successfully."
 * }
 *
 * // Response
 * {
 *   "message": "Check-out recorded successfully",
 *   "data": {
 *     "type": "checkOut",
 *     "employeeId": "12345",
 *     "comment": "Employee checked out successfully.",
 *     "creationDate": "2023-01-01T14:00:00.000Z"
 *   }
 * }
 */
const checkOut = async (req, res) => {
  try {
    // Check for validation errors
    const errors = validationResult(req);
    if (errors && !errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    const { employeeId, comment } = req.body;

    // Find the employee by employeeId
    const employee = await Employee.findOne({ id: employeeId });

    if (!employee) {
      return res.status(404).json({
        code: HTTP_RESPONSE_CODES[404],
        error: RESPONSE_MESSAGES.EMPLOYEE_NOT_FOUND,
      });
    }

    // Get the current day in formatted date
    const currentDay = currentDayFormatted();

    // Find the last check-in / checkout record for the given employee
    const lastCheckInOutRecord = await CheckIn.find({
      employeeId,
      creationDate: { $gte: new Date(`${currentDay}T00:00:00.000Z`) }, // Check for records on the current day
    }).sort({ creationDate: -1 }); // Find the latest check-in / checkout

    if (!lastCheckInOutRecord) {
      return res.status(404).json({
        code: HTTP_RESPONSE_CODES[404],
        error: RESPONSE_MESSAGES.NO_CHECK_IN_OUT,
      });
    }

    if (lastCheckInOutRecord.type === GLOBAL_CONSTANTS.CHECKOUT) {
      return res.status(404).json({
        code: HTTP_RESPONSE_CODES[404],
        error: RESPONSE_MESSAGES.ALREADY_CHECKED_OUT,
      });
    }

    // Calculate the duration between check-in and check-out in milliseconds
    const checkInTime = lastCheckInOutRecord.creationDate;
    const checkOutTime = new Date();
    const duration = checkOutTime - checkInTime;

    // Update the workHours attribute of the employee
    employee.workHours = millisecondsToHours(duration);

    // Create a check-out record
    const newCheckOut = new CheckIn({
      type: GLOBAL_CONSTANTS.CHECKOUT,
      employeeId,
      comment,
    });

    // Save the check-out record to DB
    const savedCheckOut = await newCheckOut.save();

    // Update Work hours of the employee
    await Employee.updateOne(
      { _id: employee._id },
      { $set: { workHours: millisecondsToHours(duration) } }
    );

    res.status(201).json({
      code: HTTP_RESPONSE_CODES[201],
      message: RESPONSE_MESSAGES.CHECK_OUT_SUCCESS,
      data: savedCheckOut,
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({
      code: HTTP_RESPONSE_CODES[500],
      error: RESPONSE_MESSAGES.INTERNAL_SERVER_ERROR,
    });
  }
};

export { checkIn, checkOut };

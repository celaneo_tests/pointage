import { validationResult } from "express-validator";

import Employee from "../models/employeeModel.js";
import { HTTP_RESPONSE_CODES, RESPONSE_MESSAGES } from "../utils/constants.js";

/**
 * Create a new employee.
 *
 * This endpoint creates a new employee record in the database with the provided details.
 * The request body must contain the following attributes: 'id', 'name', 'firstName', and 'department'.
 * Validation is performed to ensure that all required attributes are present.
 *
 * @param {Object} req - Express request object.
 *   @property {Object} body - Request body containing employee details.
 *     @property {string} id - The unique identifier for the employee.
 *     @property {string} name - The last name of the employee.
 *     @property {string} firstName - The first name of the employee.
 *     @property {string} department - The department in which the employee works.
 *
 * @param {Object} res - Express response object.
 *   @method res.status() - Set the HTTP response status code.
 *   @method res.json() - Send a JSON response.
 *
 * @returns {Object} - The new employee just created.
 *
 * @example
 * // Example 1: Create a new employee
 * POST /api/employees/create
 * Request Body:
 * {
 *   "id": "EMP001",
 *   "name": "Doe",
 *   "firstName": "John",
 *   "department": "Engineering"
 * }
 * // Response: 201 Created
 * // {
 * //   "id": "EMP001",
 * //   "name": "Doe",
 * //   "firstName": "John",
 * //   "department": "Engineering",
 * //   "dateCreated": "2023-01-01T12:00:00.000Z"
 * // }
 *
 * @example
 * // Example 2: Invalid request - Missing required attributes
 * POST /api/employees/create
 * Request Body:
 * {
 *   "id": "EMP002",
 *   "name": "Doe",
 *   "department": "Engineering"
 * }
 * // Response: 400 Bad Request
 * // {
 * //   "errors": [
 * //     {
 * //       "value": undefined,
 * //       "msg": "Invalid value",
 * //       "param": "firstName",
 * //       "location": "body"
 * //     }
 * //   ]
 * // }
 */
const createEmployee = async (req, res) => {
  try {
    // Check for validation errors
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({
        code: HTTP_RESPONSE_CODES[400],
        errors: errors.array(),
      });
    }

    const { id, name, firstName, department } = req.body;

    // Check if Employee already exists
    const employee = await Employee.findOne({ id });

    if (employee) {
      return res.status(409).json({
        code: HTTP_RESPONSE_CODES[409],
        error: RESPONSE_MESSAGES.EMPLOYEE_ALREADY_EXISTS,
      });
    }

    const newEmployee = new Employee({
      id,
      name,
      firstName,
      department,
    });

    const savedEmployee = await newEmployee.save();

    res.status(201).json({
      code: HTTP_RESPONSE_CODES[201],
      message: RESPONSE_MESSAGES.EMPLOYEE_CREATED,
      data: savedEmployee,
    });
  } catch (error) {
    res.status(500).json({
      code: HTTP_RESPONSE_CODES[500],
      error: RESPONSE_MESSAGES.INTERNAL_SERVER_ERROR,
    });
  }
};

/**
 * List employees with pagination.
 *
 * This endpoint retrieves a paginated list of employees from the database. The pagination
 * parameters 'page' and 'limit' can be provided in the query string to control the number
 * of records per page and the page number to retrieve.
 * The dateCreated field is a filter on the employee creation date.
 *
 * @param {Object} req - Express request object.
 * @param {Object} res - Express response object.
 * @returns {Object} - An object that contains pagination params with employees list.
 */
const listEmployees = async (req, res) => {
  try {
    const { page = 1, limit = 10, dateCreated } = req.query || {};

    const pageNumber = parseInt(page);
    const limitNumber = parseInt(limit);

    const skip = (pageNumber - 1) * limitNumber;

    // Build a filter object based on the provided dateCreated date
    const dateFilter = dateCreated ? { dateCreated } : {};

    const employees = await Employee.find(dateFilter)
      .skip(skip)
      .limit(limitNumber)
      .exec();

    const totalEmployees = await Employee.countDocuments(dateFilter);

    // Prepare Response Object
    const response = {
      totalEmployees,
      page: pageNumber,
      limit: limitNumber,
      employees,
    };

    res.status(200).json({
      code: HTTP_RESPONSE_CODES[200],
      data: response,
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({
      code: HTTP_RESPONSE_CODES[500],
      error: RESPONSE_MESSAGES.INTERNAL_SERVER_ERROR,
    });
  }
};

export { createEmployee, listEmployees };

/**
 * Format the given date or the current date in YYYY-MM-DD format.
 *
 * @function
 * @param {Date} [date=new Date()] - The date to format. Defaults to the current date.
 * @returns {string} - The formatted date in YYYY-MM-DD format.
 *
 * @description
 * This function takes an optional date parameter and returns a string representing
 * the date in the format YYYY-MM-DD. If no date is provided, it defaults to the current date.
 *
 * @example
 * // Get the formatted current date
 * const formattedDate = currentDayFormatted();
 * console.log(formattedDate); // Outputs something like "2023-12-17"
 *
 * // Get the formatted date for a specific date
 * const specificDate = new Date("2023-01-15");
 * const formattedSpecificDate = currentDayFormatted(specificDate);
 * console.log(formattedSpecificDate); // Outputs "2023-01-15"
 */
export const currentDayFormatted = (date = new Date()) => {
  const year = date.getFullYear();
  const month = String(date.getMonth() + 1).padStart(2, "0");
  const day = String(date.getDate()).padStart(2, "0");
  return `${year}-${month}-${day}`;
};

/**
 * Convert milliseconds to hours.
 *
 * @function
 * @param {number} milliseconds - The duration in milliseconds.
 * @returns {number} - The duration in hours.
 *
 * @example
 * const durationInMilliseconds = 3600000; // 1 hour in milliseconds
 * const durationInHours = millisecondsToHours(durationInMilliseconds);
 * console.log(durationInHours); // Outputs 1
 */
export const millisecondsToHours = (milliseconds) => {
  const result = milliseconds / (1000 * 60 * 60); // Convert milliseconds to hours
  return result.toFixed(2, ".");
};

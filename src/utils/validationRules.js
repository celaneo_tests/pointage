import { body } from "express-validator";
import { VALIDATION_MESSAGES } from "./constants.js";

export const employeeValidationRules = [
  body("id").notEmpty().withMessage(VALIDATION_MESSAGES.ID),
  body("name").notEmpty().withMessage(VALIDATION_MESSAGES.NAME),
  body("firstName").notEmpty().withMessage(VALIDATION_MESSAGES.FIRST_NAME),
  body("department").notEmpty().withMessage(VALIDATION_MESSAGES.DEPARTMENT),
];

export const checkInOutValidationRules = [
  body("employeeId").notEmpty().withMessage(VALIDATION_MESSAGES.EMPLOYEE_ID),
  body("comment").notEmpty().withMessage(VALIDATION_MESSAGES.COMMENT),
];

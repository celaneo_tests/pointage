export const GLOBAL_CONSTANTS = {
  CHECK_IN: "CheckIn",
  CHECKOUT: "CheckOut",
  EMPLOYEE: "Employee",
};

export const VALIDATION_MESSAGES = {
  // Employee Validation
  ID: "ID is required",
  NAME: "Name is required",
  FIRST_NAME: "First Name is required",
  DEPARTMENT: "Department is required",

  // CheckIn Validation
  EMPLOYEE_ID: "EmployeeId is required",
  COMMENT: "Comment is required",
};

export const HTTP_RESPONSE_CODES = {
  // Successful responses (2xx)
  200: "OK",
  201: "Created",

  // Redirection messages (3xx)
  301: "Moved Permanently",

  // Client error responses (4xx)
  400: "Bad Request",
  403: "Forbidden",
  404: "Not Found",
  409: "Conflict",

  // Server error responses (5xx)
  500: "Internal Server Error",
};

export const RESPONSE_MESSAGES = {
  // Global
  CREATED: "Created",
  INTERNAL_SERVER_ERROR: "Internal Server Error",
  DB_CONNECTION_SUCCESS: "Connected Successfully to MongoDB",
  DB_CONNECTION_FAIL: "Error connecting to MongoDB ",

  // Employee
  EMPLOYEE_CREATED: "Employee recorded successfully",
  EMPLOYEE_NOT_FOUND: "Employee with the given ID is not found",
  EMPLOYEE_ALREADY_EXISTS: "Employee already exists",

  // CheckIn - CheckOut
  CHECK_IN_SUCCESS: "Check-in recorded successfully",
  CHECK_OUT_SUCCESS: "Check-out recorded successfully",
  ALREADY_CHECKED_OUT: "Employee already checked out",
  NO_CHECK_IN_OUT:
    "No check-in/checkout record found for the employee. Try to Check-in first",
};

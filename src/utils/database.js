import mongoose from "mongoose";
import { RESPONSE_MESSAGES } from "./constants.js";

const connectDB = async () => {
  const uri = process.env.MONGODB_URI;

  if (!uri) {
    console.error("MONGODB_URI is not defined in the environment variables.");
    return;
  }

  try {
    await mongoose.connect(uri);
    console.log(RESPONSE_MESSAGES.DB_CONNECTION_SUCCESS);
  } catch (error) {
    console.error(RESPONSE_MESSAGES.DB_CONNECTION_FAIL, error.message);
  }
};

const closeDatabaseConnection = async () => {
  await mongoose.connection.close();
};

export { mongoose, connectDB, closeDatabaseConnection };

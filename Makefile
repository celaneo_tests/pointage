DOCKER_USERNAME ?= celaneo
APPLICATION_NAME ?= pointage
GIT_HASH ?= $(shell git log --format="%h" -n 1)
 
build:
		docker build --tag ${DOCKER_USERNAME}/${APPLICATION_NAME}:${GIT_HASH} .

run:
		docker run ${DOCKER_USERNAME}/${APPLICATION_NAME}:${GIT_HASH} .

# TODO to be changed with real docker hub credentials
push:
		docker push ${DOCKER_USERNAME}/${APPLICATION_NAME}:${GIT_HASH}

release:
		docker pull ${DOCKER_USERNAME}/${APPLICATION_NAME}:${GIT_HASH}
		docker tag  ${DOCKER_USERNAME}/${APPLICATION_NAME}:${GIT_HASH} ${DOCKER_USERNAME}/${APPLICATION_NAME}:latest
		docker push ${DOCKER_USERNAME}/${APPLICATION_NAME}:latest
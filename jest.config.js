const testEnvironment = "node";
const testMatch = ["**/*.test.js"];
const moduleFileExtensions = ["js", "json"];
const testPathIgnorePatterns = ["/node_modules/"];
const setupFiles = ["dotenv/config"];
const testTimeout = 10000;
const testSequencer = "./config/testSequencer.js";

export default {
  testEnvironment,
  testMatch,
  moduleFileExtensions,
  testPathIgnorePatterns,
  setupFiles,
  testTimeout,
};

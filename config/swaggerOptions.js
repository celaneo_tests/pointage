const swaggerOptions = {
  swaggerDefinition: {
    openapi: "3.0.0",
    info: {
      title: "Api de pointage",
      version: "1.0.0",
      description: "Application de pointage des employés d'une école",
    },
    servers: [
      {
        url: "http://localhost:3000",
        description: "Development server",
      },
    ],
  },
  apis: ["./src/routes/*.js"], // Path to the files containing your API routes
};

export default swaggerOptions;
